<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rules', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedInteger('priority');
            $table->boolean('status');
            $table->string('textcolor')->nullable();
            $table->string('textbgcolor')->nullable();
            $table->string('allproducts')->nullable();
            $table->string('alltags')->nullable();
            $table->unsignedInteger('textfontsize')->nullable();
            $table->string('texttype');
            $table->string('products')->nullable();
            $table->string('collections')->nullable();
            $table->string('usertype');
            $table->string('tags')->nullable();
            $table->string('text');
            $table->string('shop_id');
            $table->string('shop_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rules');
    }
}

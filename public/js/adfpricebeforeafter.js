$('.postName').select2({
    placeholder: 'Select an item',
    ajax: {
        url: '/product-search',
        dataType: 'json',
        delay: 500,
        data: function (data) {
            return {
                searchTerm: data.term // search term
            };
        },
        processResults: function (response) {
            return {
                results:response.searchResult
            };
        },
    }
});

$('.tags').select2({
    placeholder: 'Enter a tag',
    ajax: {
        url: '/tag-search',
        dataType: 'json',
        delay: 500,
        data: function (data) {
            return {
                searchTerm: data.term // search term
            };
        },
        processResults: function (response) {
            return {
                results:response.searchResult
            };
        },
    }
});
  
$('#ruleData').submit(function(e) {

    let type = $(this).attr('do');  

    e.preventDefault();
    console.log('heee');
    $('.adf-spinner').addClass("rule-submit--loading");
    $('.adf-submit-btn').addClass("disabled");

    let formData = new FormData(this);

     $.ajax({
          type: 'POST',
          data : formData,
          contentType: false,
           processData: false,
           url: '/store-rule',
           beforeSend:function(){

            },

            complete: function(){

            },

            success: function(res){

                if(res.success == true){
                    $('.adf-spinner').removeClass("rule-submit--loading");
                    $('#ruleData').trigger("reset");
                    $(".postName").empty();
                    $(".tags").empty();
                    $('.adf-submit-btn').removeClass("disabled");
                    $('.adf-success').show();
                    $('.adf-success-text').text(res.message);
                    let top = document.getElementById("adf-rule-add");
                    top.scrollIntoView();
                }
            },

            error(err){
                $.each(err.responseJSON,function(key,val) {
                    $(".adf-error-text").html(null);
                    $('.row').removeClass("error");
                    if(key == 'errors') {
                        
                        $.each(val, function( index, value ) {
                            $('.adf-'+index+'-error').text(value);
                            $('.adf-row-'+index).addClass("error");
                          });
                    }
                
                })
                $('.adf-spinner').removeClass("rule-submit--loading");
                $('.adf-submit-btn').removeClass("disabled");

            }
    })
});

$('#ruleUpdateData').submit(function(e) {

    let type = $(this).attr('do');  
    var id= document.getElementById('rule_id').value;
    e.preventDefault();
    console.log('heee');
    $('.adf-spinner').addClass("rule-submit--loading");
    $('.adf-submit-btn').addClass("disabled");

    let formData = new FormData(this);

     $.ajax({
          type: 'POST',
          data : formData,
          contentType: false,
           processData: false,
           url: '/rule/'+id+'/update',
           beforeSend:function(){

            },

            complete: function(){

            },

            success: function(res){

                if(res.success == true){
                    $('.adf-spinner').removeClass("rule-submit--loading");
                    $('.adf-submit-btn').removeClass("disabled");
                    $('.adf-success').show();
                    $('.adf-success-text').text(res.message);
                    let top = document.getElementById("adf-rule-update");
                    top.scrollIntoView();
                }
            },

            error(err){
                $.each(err.responseJSON,function(key,val) {
                    $(".adf-error-text").html(null);
                    $('.row').removeClass("error");
                    if(key == 'errors') {
                        
                        $.each(val, function( index, value ) {
                            $('.adf-'+index+'-error').text(value);
                            $('.adf-row-'+index).addClass("error");
                          });
                    }
                
                })
                $('.adf-spinner').removeClass("rule-submit--loading");
                $('.adf-submit-btn').removeClass("disabled");

            }
    })
});

$(document).on('click', '.adf-allproducts-check', function(){
    if(this.checked){
        $('.adf-main-products').hide();
        $('.adf-main-collections').hide();
    } else {
        $('.adf-main-products').show();
        $('.adf-main-collections').show();
    }
});

$(document).on('click', '.adf-registered-customer', function(){
    if(this.checked){
        $('.adf-main-tags').show();
        let radioC = $("input[name='alltags']:checked").val();
        if(radioC == 2 ){
            $('.adf-main-tags-field').show();
        }
    } else {
        $('.adf-main-tags').hide();
        $('.adf-main-tags-field').hide();
    }
});
$(document).on('click', '.adf-registered-tags', function(){
    if(this.value == 2 ){
        $('.adf-main-tags-field').show();
    } else {
        $('.adf-main-tags-field').hide();
    }
});

$(document).on('click', '.adf-transparentbg-check', function(){
    if(this.checked){
        $('.adf-bgcolor-main').hide();
    }else{
        $('.adf-bgcolor-main').show();
    }
});

$( document ).ready(function() {

    var transparentBg = document.getElementsByClassName('adf-transparentbg-check');
    if(transparentBg.length >= 1){
        if(transparentBg[0].checked){
            $('.adf-bgcolor-main').hide();
        } 
    } 

    var allProducts = document.getElementsByClassName('adf-allproducts-check');
    if(allProducts.length >= 1){
        if(allProducts[0].checked){
            $('.adf-main-products').hide();
            $('.adf-main-collections').hide();
        } 
    }  

    var regTags = $("input[name='alltags']:checked").val();
    if(regTags == 2 ){
        $('.adf-main-tags-field').show();
    } else {
        $('.adf-main-tags-field').hide();
    }

    var mainTags = document.getElementsByClassName('adf-registered-customer');
    if(mainTags.length >= 1){
        if(!mainTags[0].checked){
            $('.adf-main-tags').hide();
            $('.adf-main-tags-field').hide();
        } 
    }

  });

$('#checkbox1').change(function() {
    if(this.checked) {
        var returnVal = confirm("Are you sure?");
        $(this).prop("checked", returnVal);
    }
    $('#textbox1').val(this.checked);        
});
$(document).on('click', '#adf-alert-success', function(){
    $('.adf-success').hide();
});
console.log("here");

$( "#rule-search" ).keyup(function() {
    ajaxRuleSearch($( this ).val());
});
var request;
function ajaxRuleSearch(searchKey) {
    /* if request is in-process, kill it */
    if(request) {
        request.abort();
    };
        request = $.ajax({
            type:'GET',
            dataType: 'JSON',
            url: '?search='+searchKey,
          
            success:function(response){
              if(response.success == true){
                console.log(response); 
                var rules = response.rules.data;
  
                let rulesView = '';
                
                for(let i = 0; i < rules.length; i++){
                    let textType = '';
                    if(rules[i].texttype == 1){
                      textType = "Before Price";
                    } else{
                      textType = "After Price";
                    }
                    rulesView += `
                        <tr>
                        <td>`+rules[i].title+`</td>
                        <td>`+rules[i].priority+`</td>
                        <td>`+textType+`</td>
                        <td>`+rules[i].text+`</td><td>`;
                        if(rules[i].status == 1){
                            rulesView += `<span class="tag green">Active</span>`;
                        } else {
                            rulesView += `<span class="tag red">Draft</span>`;
                        }

                    rulesView += `</td><td><a href="`+baseURL+`rule/`+rules[i].id+`/edit" class="button secondary icon-edit"></a>
                                <button data-id="`+rules[i].id+`" class="secondary adf-del-btn icon-trash rule-delete"></button></td>
                        </tr> `;
                }
                $('#rules-tbody').html(rulesView);
                $('.pagination').html(response.pagination);
                if(rules.length == 0){
                    $('#rules-tbody').html('<tr><th colspan="6" style="text-align: center;">No Rule Found</th><tr>');
                }
              }
          
            },
          
            error:function(err){
              console.log(err);
            }
        });
}




// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
$(document).on('click', '.adf-del-btn', function(){
    modal.style.display = "block";
    let ruleID = $(this).data('id');
    let confirmBtn = document.getElementById('adf-confirm-del');
    confirmBtn.setAttribute("data-id", ruleID);
});

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("adf-close")[0];

// When the user clicks the button, open the modal 
$(document).on('click', '.adf-close', function(){
    modal.style.display = "none";
});

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


const baseURL = 'https://addifypricebeforeafter.test/';

$(document).on('click', '.adf-con-delete-btn', function(){

  let ruleID = $(this).attr('data-id');

  $.ajax({

  type:'GET',
  dataType: 'JSON',
  url: '/rule/'+ruleID+'/destroy',

  success:function(response){
    if(response.success == true){
      refreshRules();
      $('.adf-del-success').show();
      $('.adf-success-text').text(response.message);
      let modal = document.getElementById("myModal");
      modal.style.display = "none";
    }

  },

  error:function(err){
    console.log(err);
  }

  });
});

function refreshRules(){

  console.log('refresh');

  $.ajax({
    type : 'GET',
    dataType: "json",
    url  : '/',
    success : function (res) {
          //console.log(res);
          table_post_row(res);
    },error : function(error){
      console.log(error);
    }
  });
}




function table_post_row(res){

  console.log('mainrefresh');
  console.log(res);
  var rules = res.rules.data;
  
  let rulesView = '';

  for(let i = 0; i < rules.length; i++){
    let textType = '';
    if(rules[i].texttype == 1){
      textType = "Before Price";
    } else{
      textType = "After Price";
    }
    rulesView += `
          <tr>
          <td>`+rules[i].title+`</td>
          <td>`+rules[i].priority+`</td>
          <td>`+textType+`</td>
          <td>`+rules[i].text+`</td><td>`;
          if(rules[i].status == 1){
            rulesView += `<span class="tag green">Active</span>`;
          } else {
            rulesView += `<span class="tag red">Draft</span>`;
          }

    rulesView += `</td><td><a href="`+baseURL+`rule/`+rules[i].id+`/edit" class="button secondary icon-edit"></a>
                  <button data-id="`+rules[i].id+`" class="secondary adf-del-btn icon-trash rule-delete"></button></td>
          </tr> `;
  }

  $('#rules-tbody').html(rulesView);
  $('.pagination').html(res.pagination);
  if(rules.length == 0){
    $('#rules-tbody').html('<tr><th colspan="6" style="text-align: center;">No More Rules</th><tr>');
  }

}

$(document).on('click', '#adf-alert-success-del', function(){
  $('.adf-del-success').hide();
});

$(document).on('change', '#adf-status', function(){
  
  $.ajax({
    type : 'GET',
    dataType: "json",
    url  : '/status-update?status='+$( this ).val(),
    success : function (res) {
      var x = document.getElementById("adf-status-snackbar");
      x.className = "adf-status-snackbar-show";
      setTimeout(function(){ x.className = x.className.replace("adf-status-snackbar-show", ""); }, 2000);
    },error : function(error){
      console.log(error);
    }
  });  
});


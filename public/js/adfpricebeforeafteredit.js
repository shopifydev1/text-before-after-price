if(document.getElementById('rule_id')){
    const ruleID = document.getElementById('rule_id').value;
    const ajaxTimeout = setTimeout(ruleProducts, 1000);

    var studentSelect = $('.postName');
    var tagSelect = $('.tags');

    function ruleProducts() {
        $.ajax({
            type: 'GET',
            contentType: false,
            processData: false,
            url: baseURL+'selected-rule-products?id='+ruleID

        }).then(function (data) {

            var result  = data.searchResult;
            result.forEach(function(item) {
                var option = new Option(item.text, item.id, true, true);
                studentSelect.append(option).trigger('change');
            });
        
        });

        $.ajax({
            type: 'GET',
            contentType: false,
            processData: false,
            url: baseURL+'selected-rule-tags?id='+ruleID

        }).then(function (data) {

            var result  = data.searchResult;
            result.forEach(function(item) {
                var option = new Option(item.text, item.id, true, true);
                tagSelect.append(option).trigger('change');
            }); 
        
        });
    }
}


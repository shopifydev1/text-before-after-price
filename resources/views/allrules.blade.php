@extends('shopify-app::layouts.default')

@section('content')
<main>

<section>
</section>
    <section>

    <div class="columns seven">
        <h2><strong>Rules</strong></h2>
    </div>

    <div class="columns five align-right">
        <select style="width:32%;display: inline-block;" id="adf-status">
            <option value="1" @if ($appStatus == 1) selected @endif>Active App</option>
            <option value="0" @if ($appStatus == 0) selected @endif>Disable App</option>
        </select>
        <a href="{{ route('guide') }}" class="button adf-submit-btn">Guide</a>      
        <a href="{{ route('rule.create') }}" class="button adf-submit-btn">Add Rule</a>         
    </div>

    </section>

    <section class= "adf-del-success" style="display:none;">
    <div class="alert success adf-success" style="width:100%;">
        <a class="close" href="#" id="adf-alert-success-del"></a>
        <dl>
        <dt class="adf-success-text"></dt>
        </dl>
      </div>
      </section>

<!-- <button id="adf-del-btn">Open Modal</button> -->
<div id="myModal" class="modal">
<div class="modal-content">
  <div class="modal-header">
    <span class="close adf-close">&times;</span>
    <h2 style="margin:0px;">Remove 1 rule?</h2>
  </div>
  <div class="modal-body">
    <p style="margin:0px;">This can’t be undone.</p>
  </div>
  <div class="modal-footer">
  <button class="secondary adf-close">Cancel</button>
  <button class="adf-con-delete-btn" id="adf-confirm-del">Delete</button>

  </div>
</div> 
 </div>
<div id="adf-status-snackbar">Status Updated</div>

    <section>
    <input type="search" name="ruleSearch" id="rule-search" class="target" value="{{ $searchQuery ?? '' }}" placeholder="Search by Title"/> 
    </section>



    <section>

        <div class="card">
       
        <table>
            <thead>
                <tr>
                <th>Title</th>
                <th>Priority    </th>
                <th>Position</th>
                <th>Text</th>
                <th>Status</th>
                <th>Action</th>
                
                </tr>
            </thead>
            <tbody id="rules-tbody"> 
        @if (!$rules->isEmpty())
            @foreach ($rules as $rule)
                <tr>
                <td> {{ $rule->title }}</td>
                <td>{{ $rule->priority }}</td>
                <td>@if ($rule->texttype == 1) Before Price @else After Price @endif</td>
                <td>{{ $rule->text }}</td>
                <td>@if ($rule->status == 1) <span class="tag green">Active</span> @else <span class="tag red">Draft</span> @endif</td>
                <td><a href="{{ route('rule.edit',$rule->id) }}" class="button secondary icon-edit"></a>
                <button data-id="{{ $rule->id }}" class="secondary icon-trash adf-del-btn rule-delete"></button></td>
                
                </tr>
            @endforeach
        @else
            <tr><th colspan="6" style="text-align: center;">No Rule Found</th><tr>
        @endif
            </tbody>
        </table>
        @if ($rules->hasPages())
            <div class="pagination">
            <span class="button-group">
            @if ($rules->onFirstPage())
                <button type="button" class="disabled secondary icon-prev"></button>
            @else
                <a href="{{ $rules->previousPageUrl() }}" rel="prev"><button type="button" class="secondary icon-prev"></button></a>
            @endif
            @if ($rules->hasMorePages())
                <a href="{{ $rules->nextPageUrl() }}" rel="next"> <button type="button" class="secondary icon-next"></button></a>
            @else
                <button type="button" class="disabled secondary icon-next"></button>
            @endif
            </span>
            </div>
        @endif

        </div>
        </section>

</main>
<footer>
  <article class="help">
    <span></span>
    <p>Learn more about <a href="#">%screen%</a> at the <a href="#">%company%</a> Help Center.</p>
  </article>
</footer>
@endsection


@section('scripts')
    @parent
    <script>
        actions.TitleBar.create(app, { title: 'Rules' });
    </script>
@endsection
@extends('shopify-app::layouts.default')

@section('content')

<main id="adf-rule-add">
    <section>

    </section>

    <section>
        <div class="columns one" style = "width: 5%;">
            <a href="{{ route('home') }}" class="button secondary icon-arrow-left"></a>
        </div>

        <div class="columns eleven">
            <h2><strong>Add Rule</strong></h2>
        </div>
            
    </section>

  <form id="ruleData">
      <section id="adf-success-alert">
  <div class="alert success adf-success" style="width:100%;display:none;">
  <a class="close" href="#" id="adf-alert-success"></a>
  <dl>
    <dt class="adf-success-text"></dt>
    
  </dl>
</div>
</section>
    <section>
        
    <div class="card columns eight">
    <div class="row adf-row-text">
                <label id="adf-label">Text to display</label>
                <input type="text" name="text"/>
                <p class="adf-text-error adf-error-text"></p>
            </div>
    <div class="row">
        <label id="adf-label">Position of the text</label>
                <label><input type="radio" name="texttype" checked="checked" value="1">Before Price</label>
                <label><input type="radio" name="texttype" value="2">After Price</label>
            </div>
            <div class="row">
                <label id="adf-label">Size of text</label>
                <div class="input-group">
                <input type="number" name="textfontsize"/>
                <span class="append">px</span>
                </div>
            </div>
            <div class="row">
                <label id="adf-label">Text Color</label>
                <input type="color" name="textcolor"/>
            </div>
            <div class="row">
                <label id="adf-label"><input type="checkbox" class="adf-transparentbg-check" name="transparentbg" value="1">Default Background Color</label>
            </div>
            <div class="row adf-bgcolor-main">
                <label id="adf-label">Text Background Color</label>
                <input type="color" name="textbgcolor"/>
            </div>
  </div>
  <div class="card columns four">
  <h5>Rule Settings</h5>
  <div class="row">
                <select name="status">
                    <option value="1">Active</option>
                    <option value="0">Draft</option>
                </select>
            </div>
            <div class="row adf-row-title">
                <label id="adf-label">Title</label>
                <input type="text" name="title"/>
                <p class="adf-title-error adf-error-text"></p>
            </div>
            <div class="row adf-row-priority">
                <label id="adf-label">Priority</label>
                <input type="number" min=0 name="priority"/>
                <p class="adf-priority-error adf-error-text"></p>
            </div>
            <div class="row adf-row-priority">
                <label id="adf-label">Rule Works On</label>
                 <select name="rulefor">
                    <option value="1">Product Page</option>
                    <option value="2">Products Listing</option>
                    <option value="3">Whole Shop</option>
                </select>
            </div>
            <div class="align-right">
                <span class="adf-spinner"></span>
                <button class="adf-submit-btn" type="submit">Save</button>
            </div>
  </div>
    </section>

    <section>
        <div class="card">
        <h5>Rule Works For</h5>
            <div class="row">
                <label id="adf-label"><input type="checkbox" class="adf-allproducts-check" name="allproducts" value="1">All Products</label>
            </div>
            <div class="row adf-main-products">
                <label id="adf-label">Products</label>
                <select class="postName form-control" style="width:500px" name="products[]" multiple="multiple"></select>
            </div>
            <div class="row adf-main-collections">
                <label id="adf-label">Select Collections</label>
                @foreach ($collections['edges'] as $collection)
                    <label><input type="checkbox" name="collections[]" value="{{ $collection['node']['id'] }}">{{ $collection['node']['title']  }}</label>
                @endforeach
            </div>
            <div class="row">
                <label id="adf-label">Display Text For</label>
                <label><input type="checkbox" name="usertype[]" value="1">Guest Customers</label>
                <label><input type="checkbox" name="usertype[]" class= "adf-registered-customer" value="2">All Registered Customers</label>
            </div>
            <div class="row adf-main-tags">
                <label><input type="radio" class="adf-registered-tags" name="alltags" value="1" checked>Customers with any tag</label>
                <label><input type="radio" class="adf-registered-tags" name="alltags" value="2">Customers with specific tags</label>
            </div>
            <div class="row adf-main-tags-field">
                <label id="adf-label">Tags</label>
                <select class="tags form-control" style="width:500px" name="tags[]" multiple="multiple"></select>
            </div>
        </div>           
    </section>
  </form>
</main>

@endsection


@section('scripts')
    @parent
    <script>
        actions.TitleBar.create(app, { title: 'Add Rule' });
    </script>
@endsection
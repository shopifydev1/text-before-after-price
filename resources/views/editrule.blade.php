@extends('shopify-app::layouts.default')

@section('content')

<main>
    <section>

    </section>

    <section>
        <div class="columns one" style = "width: 5%;">
            <a href="{{ route('home') }}" class="button secondary icon-arrow-left"></a>
        </div>

        <div class="columns eleven">
            <h2><strong>Edit Rule</strong></h2>
        </div>
            
    </section>

  <form id="ruleUpdateData">

    <input type="hidden" id="rule_id" value="{{ $rule->id }}">


    <section id="adf-rule-update">
        <div class="alert success adf-success" style="width:100%;display:none;">
        <a class="close" href="#" id="adf-alert-success"></a>
        <dl>
        <dt class="adf-success-text"></dt>
        </dl>   
        </div>
    </section>

    <section>
        
    <div class="card columns eight">
    <div class="row adf-row-text">
                <label id="adf-label">Text to display</label>
                <input type="text" name="text" value="{{ $rule->text}}"/>
                <p class="adf-text-error adf-error-text"></p>
            </div>
            <div class="row">
                <label id="adf-label">Position of the text</label>
                <label><input type="radio" name="texttype" @if ($rule->texttype == 1) checked="checked" @endif value="1">Before Price</label>
                <label><input type="radio" name="texttype" @if ($rule->texttype == 2) checked="checked" @endif value="2">After Price</label>
            </div>
            <div class="row">
                <label id="adf-label">Size of text</label>
                <div class="input-group">
                <input type="number" name="textfontsize" value="{{ $rule->textfontsize}}"/>
                <span class="append">px</span>
                </div>
            </div>
            <div class="row">
                <label id="adf-label">Text Color</label>
                <input type="color" name="textcolor" value="{{ $rule->textcolor}}"/>
            </div>
            <div class="row">
                <label id="adf-label"><input type="checkbox" class="adf-transparentbg-check" name="transparentbg" value="1" @if ($rule->transparentbg == 1) checked @endif>Default Background Color</label>
            </div>
            <div class="row adf-bgcolor-main">
                <label id="adf-label">Text Background Color</label>
                <input type="color" name="textbgcolor" value="{{ $rule->textbgcolor}}"/>
            </div>
  </div>
  <div class="card columns four">
  <h5>Rule Settings</h5>
  <div class="row">
                <select name="status">
                    <option value="1" @if ($rule->status == 1) selected @endif>Active</option>
                    <option value="0" @if ($rule->status == 0) selected @endif>Draft</option>
                </select>
            </div>
            <div class="row adf-row-title">
                <label id="adf-label">Title</label>
                <input type="text" name="title" value="{{ $rule->title}}"/>
                <p class="adf-title-error adf-error-text"></p>
            </div>
            <div class="row adf-row-priority">
                <label id="adf-label">Priority</label>
                <input type="number" min=0 name="priority" value="{{ $rule->priority}}"/>
                <p class="adf-priority-error adf-error-text"></p>
            </div>
            <div class="row adf-row-priority">
                <label id="adf-label">Rule Works On</label>
                 <select name="rulefor">
                    <option value="1" @if ($rule->rulefor == 1) selected @endif>Product Page</option>
                    <option value="2" @if ($rule->rulefor == 2) selected @endif>Products Listing</option>
                    <option value="3" @if ($rule->rulefor == 3) selected @endif>Whole Shop</option>
                </select>
            </div>
            <div class="align-right">
                <span class="adf-spinner"></span>
                <button class="adf-submit-btn" type="submit">Save</button>
            </div>
  </div>
    </section>

    <section>
        <div class="card">
        <h5>Rule Works For</h5>
    
            <div class="row">
                <label id="adf-label"><input type="checkbox" class="adf-allproducts-check" name="allproducts" value="1" @if ($rule->allproducts == 1) checked @endif>All Products</label>
            </div>

            <div class="row adf-main-products">
                <label id="adf-label">Products</label>
                <select class="postName form-control" style="width:500px" name="products[]" multiple="multiple"></select>
            </div>
            <div class="row adf-main-collections">
                <label id="adf-label">Select Collections</label>
                @php
                    $cols = explode(",",$rule->collections);
                @endphp

                @foreach ($collections['edges'] as $collection)
                    <label><input type="checkbox" name="collections[]" value="{{ $collection['node']['id'] }}" @if (in_array( $collection['node']['id'],$cols)) checked @endif>{{ $collection['node']['title']  }}</label>
                @endforeach
            </div>
            <div class="row">
                @php
                    $userType = explode(",",$rule->usertype);
                @endphp
                <label id="adf-label">Display Text For</label>
                <label><input type="checkbox" name="usertype[]" value="1" @if (in_array( 1,$userType )) checked @endif>Guest Customers</label>
                <label><input type="checkbox" class= "adf-registered-customer" name="usertype[]" value="2" @if (in_array( 2,$userType )) checked @endif>Registered Customers</label>
            </div>
            <div class="row adf-main-tags">
                <label><input type="radio" class="adf-registered-tags" name="alltags" value="1" @if ($rule->alltags == 1) checked @endif>Customers with any tag</label>
                <label><input type="radio" class="adf-registered-tags" name="alltags" value="2" @if ($rule->alltags == 2) checked @endif>Customers with specific tags</label>
            </div>
            <div class="row adf-main-tags-field">
                <label id="adf-label">Tags</label>
                <select class="tags form-control" style="width:500px" name="tags[]" multiple="multiple"></select>
            </div>
        </div>           
    </section>
  </form>
</main>

@endsection


@section('scripts')
    @parent
    <script>
        actions.TitleBar.create(app, { title: 'Edit Rule' });
    </script>
@endsection
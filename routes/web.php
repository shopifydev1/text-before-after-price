<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\PaymentPlan;
use App\Http\Controllers\RuleController;
use App\Http\Middleware\FrameHeaders;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['verify.shopify',PaymentPlan::class,FrameHeaders::class])->group(function () {
    //git
    Route::get('/',[RuleController::class,'index'])->name('home');
    Route::get('/create-rule',[RuleController::class,'create'])->name('rule.create');
    Route::post('/store-rule',[RuleController::class,'store'])->name('rule.store');
    Route::get('/rule/{id}/edit',[RuleController::class,'edit'])->name('rule.edit');
    Route::post('/rule/{id}/update',[RuleController::class,'update'])->name('rule.update');
    Route::get('/rule/{id}/destroy',[RuleController::class,'destroy'])->name('rule.destroy');
    Route::get('/product-search',[RuleController::class,'getShopProduct'])->name('rule.productsearch');
    Route::get('/selected-rule-products',[RuleController::class,'getSelectedRuleProducts'])->name('rule.selectedproducts');
    Route::get('/selected-rule-tags',[RuleController::class,'getSelectedRuleTags'])->name('rule.selectedtags');
    Route::get('/tag-search',[RuleController::class,'getShopTag'])->name('rule.tagsearch');
    Route::get('/status-update',[RuleController::class,'setAppStatus'])->name('setstatus');
    Route::get('/guide', function () {
        return view('guide');
    })->name('guide');

});

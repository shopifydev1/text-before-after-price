<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'priority',
        'status',
        'textcolor',
        'textbgcolor',
        'allproducts',
        'alltags',
        'textfontsize',
        'texttype',
        'products',
        'collections',
        'usertype',
        'tags',
        'text',
        'shop_id',
        'shop_name',
        'rulefor',
        'transparentbg',
    ];

    public function setTagsAttribute($value){
        settype($value, 'array');
        $this->attributes['tags'] = implode(',',$value);
    }

    public function setProductsAttribute($value){
        settype($value, 'array');
        $this->attributes['products'] = implode(',',$value);
    }

    public function setCollectionsAttribute($value){
        settype($value, 'array');
        $this->attributes['collections'] = implode(',',$value);
    }

    public function setUsertypeAttribute($value){
        settype($value, 'array');
        $this->attributes['usertype'] = implode(',',$value);
    }

    public function scopeActive($query){
        $query->where('status', 1);
    }

    public function scopeUnregisteredCustomer($query){
        $query->whereRaw('FIND_IN_SET(1,usertype)');
    }

    public function scopeRegisteredCustomer($query){
        $query->whereRaw('FIND_IN_SET(2,usertype)');
    }


}

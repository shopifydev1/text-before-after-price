<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Statu;

class FrontController extends Controller
{
    public function getProductpageRule(Request $request){
    
        $resultRules = [];
        $shop = User::where('name', $request->shopName)->first();

        if($shop->trashed()){
            return response()->json([
                'success' => true,
                'rule' => 'trashed',    
            ]);
        }

        $status = Statu::where('shop_name','LIKE' ,$shop->name)->first();
        if($status->status == 0){
            return response()->json([
                'success' => false,
                'rule' => 'No Rule',    
            ]); 
        }
        //Getting all collections of the product
        $collections = [];
        $shopProducts = $shop->api()->graph('
        {
            nodes(ids: "'.$request->productId.'") {
              ...on Product {
                id
                title
                collections(first: 10) {
                  edges {
                    node {
                      id
                    }
                  }
                }
              }
            }
        }');
        $shopProducts = $shopProducts['body']['data']['nodes'];
        foreach($shopProducts as $key => $shopProduct){
            foreach ($shopProduct['collections']['edges'] as $key2 => $productCollection) {
                $collections[] = $productCollection['node']['id'];
            }
        }

        //Product ID
        $product = $request->productId;

        //Customer ID
        $customer = $request->customerId;

        $rules = [];
        $tags = [];
        $customerType= '';

        if($customer != 'undefined'){
            //Customer Tags
            $shopCustomer= $shop->api()->graph('{
                customer(id: "gid://shopify/Customer/'.$request->customerId.'") {
                  tags
                }
              }');
            $tags = $shopCustomer['body']['data']['customer']['tags'];
            $rules= Rule::where('shop_name','LIKE' ,$shop->name)->active()->registeredCustomer()->get();
            $customerType= 1;
        } else{
            $rules= Rule::where('shop_name','LIKE' ,$shop->name)->active()->UnregisteredCustomer()->get();
            $customerType= 2;
        }
    
        foreach ($rules as $key => $rule) {
            
            if($rule->rulefor == 2){
                continue;
            }
            
            $tagsPass = 0;

            if($rule->alltags == 1 || $customerType == 2){
                $tagsPass = 1;
            } else {
                
                $ruleTags = explode(',',$rule->tags);
                
                foreach ($tags as $key => $tag) {
                    
                    if(in_array($tag,$ruleTags)){
                        $tagsPass = 1; 
                    }
                }
            }

            

            if($tagsPass == 0){
                continue;
            }

            $productPass = 0;
            $collectionsPass = 0;

            if($rule->allproducts == 1){
                $collectionsPass = 1; 
                $productPass = 1;
            }

            if($collectionsPass == 0){
                if($rule->collections == null ){
                    $collectionsPass = 0;
                } else {
                    $ruleCollections = explode(',',$rule->collections);
    
                    foreach ($collections as $key => $collection) {
                        if(in_array($collection,$ruleCollections)){
                            $collectionsPass = 1; 
                        }
                    }
                }
            }
            
            
            if($productPass == 0){
                if($rule->products == null){
                    $productPass = 0;
                } else {
                    
                    $ruleProducts = explode(',',$rule->products);
                    if(in_array($product,$ruleProducts)){
                        $productPass = 1; 
                    }
                }
            }
            
            
            if($collectionsPass == 0 && $productPass == 0){
                continue;
            }

            if($rule->transparentbg == 1){
                $rule->textbgcolor= null;
            }

            $textBeforePass = 0;
            $textAfterPass = 0;

            if($rule->texttype == 1 && $textBeforePass == 0){
                $resultRules['before'] = $rule;
                $textBeforePass = 1;
            } elseif($rule->texttype == 2 && $textAfterPass == 0){
                $resultRules['after'] = $rule;
                $textAfterPass = 1;
            }

            if($textBeforePass == 1 && $textAfterPass == 1){
                break;
            }

        }

        return response()->json([
            'success' => true,
            'rule' => $resultRules,    
        ]);

    }


    public function getRuleofProducts(Request $request){

        $resultRules = [];
        $shop = User::where('name', $request->shopName)->first();

        if($shop->trashed()){
            return response()->json([
                'success' => true,
                'rule' => 'trashed',    
            ]);
        }


        $status = Statu::where('shop_name','LIKE' ,$shop->name)->first();
        if($status->status == 0){
            return response()->json([
                'success' => false,
                'rule' => 'No Rule',    
            ]); 
        }
        //Collections of requested handles
        $requestedProducts = explode(',',$request->products);
        $requestedProductHandles = '{';
        foreach ($requestedProducts as $key => $requestedProduct) {
            $requestedProductHandles .= 'key'.$key.': productByHandle(handle:"'.$requestedProduct.'"){
                id
                handle
                collections(first: 10) {
                  edges {
                    node {
                      id
                    }
                  }
                }
              }';
        }
        $requestedProductHandles .= '}';        
        $allProductsCollections = $shop->api()->graph($requestedProductHandles);
        $allProductsCollections=  $allProductsCollections['body']['data']['container'];
        //dd($allProductsCollections);
       //Customer ID
       $customer = $request->customerId;

        $rules = [];
        $tags = [];

        if($customer != 'undefined'){
            //Customer Tags
            $shopCustomer= $shop->api()->graph('{
                customer(id: "gid://shopify/Customer/'.$request->customerId.'") {
                    tags
                }
                }');
            $tags = $shopCustomer['body']['data']['customer']['tags'];
            $rules= Rule::where('shop_name','LIKE' ,$shop->name)->active()->registeredCustomer()->get();
            $customerType= 1;
        } else{
            $rules= Rule::where('shop_name','LIKE' ,$shop->name)->active()->UnregisteredCustomer()->get();
            $customerType= 2;
        }

        foreach ($allProductsCollections as $key => $allProductsCollection) {
            
            foreach ($rules as $key => $rule) {

                if($rule->rulefor == 1){
                    continue;
                }
                
                $tagsPass = 0;
                if($rule->alltags == 1 || $customerType == 2){
                    $tagsPass = 1;
                } else {
                    $ruleTags = explode(',',$rule->tags);
                    foreach ($tags as $key => $tag) {
                        if(in_array($tag,$ruleTags)){
                            $tagsPass = 1; 
                        }
                    }
                }
                if($tagsPass == 0){
                    continue;
                }
                
                $productPass = 0;
                $collectionsPass = 0;

                if($rule->allproducts == 1){
                    $collectionsPass = 1; 
                    $productPass = 1;
                }

                if($collectionsPass == 0){
                    $collections= [];
                    foreach ($allProductsCollection['collections']['edges'] as $key2 => $productCollection) {
                        $collections[] = $productCollection['node']['id'];
                    }
                    if($rule->collections == null){
                        $collectionsPass = 0;
                    } else {
                        $ruleCollections = explode(',',$rule->collections);

                        foreach ($collections as $key => $collection) {
                            if(in_array($collection,$ruleCollections)){
                                $collectionsPass = 1; 
                            }
                        }
                    }
                }

                //Product ID
                if($productPass == 0){
                    $product = $allProductsCollection['id'];
                    if($rule->products == null){
                        $productPass = 0;
                    } else {
                        $ruleProducts = explode(',',$rule->products);
                        if(in_array($product,$ruleProducts)){
                            $productPass = 1; 
                        }
                    }
                }
                
                
                if($collectionsPass == 0 && $productPass == 0){
                    continue;
                }

                if($rule->transparentbg == 1){
                    $rule->textbgcolor= null;
                }

                $textBeforePass = 0;
                $textAfterPass = 0;
    
                if($rule->texttype == 1 && $textBeforePass == 0){
                    $resultRules[$allProductsCollection['handle']]['before'] = $rule;
                    $textBeforePass = 1;
                } elseif($rule->texttype == 2 && $textAfterPass == 0){
                    $resultRules[$allProductsCollection['handle']]['after'] = $rule;
                    $textAfterPass = 1;
                }
    
                if($textBeforePass == 1 && $textAfterPass == 1){
                    break;
                }

            }
        }

        return response()->json([
            'success' => true,
            'rule' => $resultRules,    
        ]);    
    }
}
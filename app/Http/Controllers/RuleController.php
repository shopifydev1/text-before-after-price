<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Rule;
use App\Models\Statu;

class RuleController extends Controller
{
    /**
     * Display listing of the rules.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $shop= Auth::user();

      $status = Statu::where('shop_name','LIKE' ,$shop->name)->first();
      
      if(empty($status)){
        $statusInputs = [];
        $inputs['shop_name'] = $shop->name;
        $inputs['status'] = true;
        $setting = Statu::create($inputs);
        $appStatus = true;
      } else {
        $appStatus = $status->status;
      }
      

     if($request->search && strlen($request->search ) >= 1){
      $rules= Rule::where('shop_name','LIKE' ,$shop->name)->where('title','LIKE','%'.$request->search.'%')
               ->orderBy('id')->simplePaginate(24);
     } else{
      $rules= Rule::where('shop_name','LIKE' ,$shop->name)
               ->orderBy('id')->simplePaginate(24);
     }
      
     $rules->appends(['search' => $request->search]);
     
      if($request->ajax()){
        
        $paginationView = view('partials.pagination',compact('rules'));

        $pagination= $paginationView->render();

        return response()->json([
            'success' => true,
            'rules' => $rules,
            'pagination' => $pagination,

        ]);
      }
      $searchQuery = $request->search;
      
      return view('allrules',compact(['rules','searchQuery', 'appStatus']));
    }

    /**
     * Show the form for creating a new rule.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shop = Auth::user();
        $collectionApi = $shop->api()->graph('{
          collections (first:250){
            edges
            {
              node{
                id
                title
              }
            }
          }
        }');
        $collections = $collectionApi['body']['data']['collections'];
        return view('newrule',compact('collections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validated = $request->validate([
        'title' => 'required',
        'priority' => 'required',
        'status' => 'required',
        'texttype' => 'required',
        'usertype' => 'required',
        'text' => 'required',
      ]);

      if(!is_array($validated) && $validated->fails()){
        return response()->json($validated->errors(),400);
      } 
      $shop = Auth::user();
      $inputs = $request->all();
      $inputs['shop_id'] = $shop->name;
      $inputs['shop_name'] = $shop->name;
      $setting = Rule::create($inputs);
      return response()->json([
          'success' => true,
          'message' => 'Rule added successfully',
       ]);
      try {
        $shop = Auth::user();
        $inputs = $request->all();
        $inputs['shop_id'] = $shop->name;
        $inputs['shop_name'] = $shop->name;
        $setting = Rule::create($inputs);
        return response()->json([
            'success' => true,
            'message' => 'Employee added successfully',
         ]);
      } catch (\Throwable $th) {
        return response()->json([
            'fail' => true,
            'message' => $th,
        ]);
      }
    }

    /**
     * Edit a resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $rule = Rule::where('id',$id)->first();
      $shop = Auth::user();
      $collectionApi = $shop->api()->graph('{
        collections (first:250){
          edges
          {
            node{
              id
              title
            }
          }
        }
      }');
      $collections = $collectionApi['body']['data']['collections'];
      return view('editrule',compact('collections','rule'));
    }

    public function update(Request $request,$id){

      $validated = $request->validate([
        'title' => 'required',
        'priority' => 'required',
        'status' => 'required',
        'texttype' => 'required',
        'text' => 'required',
      ]);

      
      $rule = Rule::where('id',$id)->first();
      $data = $request->all();
    
      if(!isset($data['allproducts'])){
        $data['allproducts'] = 0;
      }
      if(!isset($data['tags'])){
        $data['tags'] = '';
      }
      if(!isset($data['products'])){
        $data['products'] = '';
      }
      if(!isset($data['collections'])){
        $data['collections'] = '';
      }
      if(!isset($data['usertype'])){
        $data['usertype'] = '';
      }
      if(!isset($data['transparentbg'])){
        $data['transparentbg'] = '';
      }
      $rule->update($data);

      return response()->json([
        'success' => true,
        'message' => 'Rule updated successfully',
     ]);

    }


    public function destroy($id)
    {
         $rule = Rule::where('id',$id)->first();

        if($rule){

            $rule->delete();

            return response()->json([
                'success' => true,
                'message' => 'Rule deleted successfully',
            ]);

        }

        return response()->json([
          'success' => false,
          'message' => 'Not Found',
        ]);
        
    }

    public function getShopProduct(Request $request){

        $shop = Auth::user();
        $shopProducts = $shop->api()->graph('{
            products(first: 10, query: "title:*'.$request->searchTerm.'*") {
              edges {
                node {
                  id
                  title
                  featuredImage {
                    transformedSrc
                  }
                }
              }
            } 
          }');

        $products = $shopProducts['body']['data']['products']['edges'];
        $searchResult = [];
        
        foreach ($products as $key => $product) {
            $searchResult[] = ['id'=>$product['node']['id'], 'text'=>$product['node']['title']]; 
        }

        return response()->json([
          'success' => true,
          'searchResult' => $searchResult,
        ]);
    }

    public function getSelectedRuleProducts(Request $request){

      $shop = Auth::user();
      $rule = Rule::where('id',$request->id)->first();
      $ruleProducts = explode (',',$rule->products);
      $ruleProducts = implode ('","',$ruleProducts);
      $shopProducts = $shop->api()->graph('query
      {
        nodes(ids: ["'.$ruleProducts.'"]) {
          ...on Product {
            id
            title
          }
        }
      }');

      $products = $shopProducts['body']['data']['nodes'];
      $searchResult = [];
        
      foreach ($products as $key => $product) {
          $searchResult[] = ['id'=>$product['id'], 'text'=>$product['title']]; 
      }

      return response()->json([
        'success' => true,
        'searchResult' => $searchResult,
      ]);

    }

    public function getSelectedRuleTags(Request $request){
      $rule = Rule::where('id',$request->id)->first();
      $tags = explode(',',$rule->tags);
      foreach ($tags as $key => $tag) {
        $searchResult[] = ['id'=>$tag, 'text'=>$tag]; 
      }

      return response()->json([
        'success' => true,
        'searchResult' => $searchResult,
      ]);

    }

    public function getShopTag(Request $request){

      $shop = Auth::user();

      $shopCustomers = $shop->api()->graph('{
        customers(first: 1, query : "tag:'.$request->searchTerm.'") {
          edges {
            node {
              id
              tags
            }
          }
        }
      }');

      $customers = $shopCustomers['body']['data']['customers']['edges'];
      $searchResult = [];
      if(isset($customers) && !empty($customers)){
        $allTags = $customers[0]['node']['tags'];
        foreach ($allTags as $key => $allTag) {
          if (stripos($allTag, $request->searchTerm) !== false) {
            $searchResult[] = ['id'=>$allTag, 'text'=>$allTag]; 
          }
        }
      }

      return response()->json([
        'success' => true,
        'searchResult' => $searchResult,
      ]);
    }

    public function setAppStatus(Request $request){

      $shop= Auth::user();
      $status = Statu::where('shop_name','LIKE' ,$shop->name)->first();
      $status->status = $request->status;
      $status->save();
      return response()->json([
        'success' => true,
      ]);

    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentPlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $shop = Auth::user();

        if (!isset($shop->plan) && empty($shop->plan)) {
            $shopApi = $shop->api()->rest('GET', '/admin/shop.json');
            if (isset($shopApi['body']) && !empty($shopApi['body'])) {
                $result = $shopApi['body']['shop'];
                $planDisplayName = strtolower($result->plan_display_name);
                $planName = strtolower($result->plan_name);

                if ($planDisplayName == 'basic shopify' || $planName == 'affiliate') {
                    return redirect()->route('billing', ['plan' => 1, 'shop' => $shop->name]);
                } elseif ($planDisplayName == 'shopify') {
                    return redirect()->route('billing', ['plan' => 2, 'shop' => $shop->name]);
                } elseif ($planDisplayName == 'advanced shopify') {
                    return redirect()->route('billing', ['plan' => 3, 'shop' => $shop->name]);
                } elseif ($planDisplayName == 'shopify plus' || $planName == 'enterprise') {
                    return redirect()->route('billing', ['plan' => 4, 'shop' => $shop->name]);
                } else {
                    return redirect()->route('billing', ['plan' => 1, 'shop' => $shop->name]);
                }
            }
        }
        return $next($request);
    }
}
